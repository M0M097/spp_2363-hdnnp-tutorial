## #############################################################
### This is the input file for RuNNer 
### #############################################################
### General remarks: 
### - commands can be switched off by using the # character at the BEGINNING of the line
### - the input file can be structured by blank lines and comment lines
### - the order of the keywords is arbitrary
### - if keywords are missing, default values will be used and written to runner.out
### - if mandatory keywords or keyword options are missing, RuNNer will stop with an error message 

### THIS INPUT.NN IS AN EXAMPLE, IT IS NOT A REALISTIC CASE
### It contains only a subset of all keywords

########################################################################################################################
### general keywords
########################################################################################################################
nn_type_short 1                                 
runner_mode 3
parallel_mode 1                         
number_of_elements 3                  
elements O H C                            
random_seed 9433
random_number_type 5                      
#remove_atom_energies                     
#atom_energy O -74.94518524              
#atom_energy H  -0.45890771             
atom_mass O 15.999 
atom_mass H 1.00784
atom_mass C 12.0107
energy_threshold 100.0d0              
bond_threshold 0.4d0                 

########################################################################################################################
### NN structure of the short-range NN  
########################################################################################################################
use_short_nn                               
global_hidden_layers_short 2              
global_nodes_short 14 14
global_activation_short t t l            

########################################################################################################################
### symmetry function generation ( mode 1): 
########################################################################################################################
test_fraction 0.1      
use_sf_groups

########################################################################################################################
### symmetry function definitions (all modes): 
########################################################################################################################
cutoff_type 1

#Short bond found     2    5    1.8386 Bohr =     0.9729  Ang H  H
symfunction_short H 2 H    0.000000      0.000000     15.000000
symfunction_short H 2 H    0.004000      0.000000     15.000000
symfunction_short H 2 H    0.009000      0.000000     15.000000
symfunction_short H 2 H    0.016000      0.000000     15.000000
symfunction_short H 2 H    0.028000      0.000000     15.000000
symfunction_short H 2 H    0.049000      0.000000     15.000000
symfunction_short H 2 H    0.094000      0.000000     15.000000
symfunction_short H 2 H    0.215000      0.000000     15.000000


#Short bond found     3    4    2.9797 Bohr =     1.5768  Ang O  O
#
#
symfunction_short O 2 O    0.000000      0.000000     15.000000
symfunction_short O 2 O    0.003000      0.000000     15.000000
symfunction_short O 2 O    0.006000      0.000000     15.000000
symfunction_short O 2 O    0.010000      0.000000     15.000000
symfunction_short O 2 O    0.015000      0.000000     15.000000
symfunction_short O 2 O    0.022000      0.000000     15.000000
symfunction_short O 2 O    0.032000      0.000000     15.000000
symfunction_short O 2 O    0.048000      0.000000     15.000000



symfunction_short C 2 C    0.000000      0.000000     15.000000
symfunction_short C 2 C    0.003747      0.000000     15.000000
symfunction_short C 2 C    0.009066      0.000000     15.000000
symfunction_short C 2 C    0.017212      0.000000     15.000000
symfunction_short C 2 C    0.030893      0.000000     15.000000
symfunction_short C 2 C    0.056909      0.000000     15.000000

#Short bond found     1    3    1.9184 Bohr =     1.0151  Ang C  O 

symfunction_short O 2 C    0.000000      0.000000     15.000000
symfunction_short O 2 C    0.004000      0.000000     15.000000
symfunction_short O 2 C    0.009000      0.000000     15.000000
symfunction_short O 2 C    0.017000      0.000000     15.000000
symfunction_short O 2 C    0.029000      0.000000     15.000000
symfunction_short O 2 C    0.052000      0.000000     15.000000
symfunction_short O 2 C    0.101000      0.000000     15.000000
symfunction_short O 2 C    0.241000      0.000000     15.000000

symfunction_short C 2 O    0.000000      0.000000     15.000000
symfunction_short C 2 O    0.004000      0.000000     15.000000
symfunction_short C 2 O    0.009000      0.000000     15.000000
symfunction_short C 2 O    0.017000      0.000000     15.000000
symfunction_short C 2 O    0.029000      0.000000     15.000000
symfunction_short C 2 O    0.052000      0.000000     15.000000
symfunction_short C 2 O    0.101000      0.000000     15.000000
symfunction_short C 2 O    0.241000      0.000000     15.000000

#Short bond found     4    5    1.0257 Bohr =     0.5428  Ang O  H
#

symfunction_short O 2 H    0.000000      0.000000     15.000000
symfunction_short O 2 H    0.004000      0.000000     15.000000
symfunction_short O 2 H    0.010000      0.000000     15.000000
symfunction_short O 2 H    0.019000      0.000000     15.000000
symfunction_short O 2 H    0.035000      0.000000     15.000000
symfunction_short O 2 H    0.067000      0.000000     15.000000
symfunction_short O 2 H    0.149000      0.000000     15.000000
symfunction_short O 2 H    0.486000      0.000000     15.000000

symfunction_short H 2 O    0.000000      0.000000     15.000000
symfunction_short H 2 O    0.004000      0.000000     15.000000
symfunction_short H 2 O    0.010000      0.000000     15.000000
symfunction_short H 2 O    0.019000      0.000000     15.000000
symfunction_short H 2 O    0.035000      0.000000     15.000000
symfunction_short H 2 O    0.067000      0.000000     15.000000
symfunction_short H 2 O    0.149000      0.000000     15.000000
symfunction_short H 2 O    0.486000      0.000000     15.000000

#Short bond found     1    2    1.1867 Bohr =     0.6280  Ang C  H

symfunction_short H 2 C    0.000000      0.000000     15.000000
symfunction_short H 2 C    0.004000      0.000000     15.000000
symfunction_short H 2 C    0.009000      0.000000     15.000000
symfunction_short H 2 C    0.018000      0.000000     15.000000
symfunction_short H 2 C    0.031000      0.000000     15.000000
symfunction_short H 2 C    0.056000      0.000000     15.000000
symfunction_short H 2 C    0.114000      0.000000     15.000000
symfunction_short H 2 C    0.296000      0.000000     15.000000

symfunction_short C 2 H    0.000000      0.000000     15.000000
symfunction_short C 2 H    0.004000      0.000000     15.000000
symfunction_short C 2 H    0.009000      0.000000     15.000000
symfunction_short C 2 H    0.018000      0.000000     15.000000
symfunction_short C 2 H    0.031000      0.000000     15.000000
symfunction_short C 2 H    0.056000      0.000000     15.000000
symfunction_short C 2 H    0.114000      0.000000     15.000000
symfunction_short C 2 H    0.296000      0.000000     15.000000





#
# angular
symfunction_short H 3 O H 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O H 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O H 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O H 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O H 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O H 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O H 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O H 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short H 3 H H 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 H H 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 H H 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 H H 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 H H 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 H H 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 H H 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 H H 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short H 3 C C 0.0  1.0 1.0   15.0 ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C C 0.0  1.0 2.0   15.0 ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C C 0.0  1.0 4.0   15.0 ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C C 0.0  1.0 16.0  15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C C 0.0  -1.0 1.0  15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C C 0.0  -1.0 2.0  15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C C 0.0  -1.0 4.0  15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C C 0.0  -1.0 16.0 15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short H 3 O O 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O O 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O O 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O O 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O O 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O O 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O O 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 O O 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short H 3 C H 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C H 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C H 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C H 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C H 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C H 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C H 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C H 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short H 3 C O 0.0  1.0 1.0   15.0 ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C O 0.0  1.0 2.0   15.0 ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C O 0.0  1.0 4.0   15.0 ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C O 0.0  1.0 16.0  15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C O 0.0  -1.0 1.0  15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C O 0.0  -1.0 2.0  15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C O 0.0  -1.0 4.0  15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short H 3 C O 0.0  -1.0 16.0 15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short O 3 O O 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O O 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O O 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O O 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O O 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O O 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O O 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O O 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short O 3 C C 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C C 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C C 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C C 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C C 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C C 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C C 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C C 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short O 3 O H 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O H 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O H 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O H 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O H 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O H 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O H 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 O H 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short O 3 C H 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C H 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C H 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C H 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C H 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C H 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C H 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C H 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short O 3 H H 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 H H 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 H H 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 H H 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 H H 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 H H 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 H H 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 H H 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short O 3 C O 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C O 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C O 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C O 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C O 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C O 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C O 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short O 3 C O 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short C 3 H H 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 H H 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 H H 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 H H 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 H H 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 H H 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 H H 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 H H 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short C 3 O O 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O O 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O O 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O O 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O O 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O O 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O O 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O O 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short C 3 O H 0.0  1.0 1.0   15.0 ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O H 0.0  1.0 2.0   15.0 ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O H 0.0  1.0 4.0   15.0 ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O H 0.0  1.0 16.0  15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O H 0.0  -1.0 1.0  15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O H 0.0  -1.0 2.0  15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O H 0.0  -1.0 4.0  15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O H 0.0  -1.0 16.0 15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short C 3 O C 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O C 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O C 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O C 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O C 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O C 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O C 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 O C 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

symfunction_short C 3 C H 0.0  1.0 1.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 C H 0.0  1.0 2.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 C H 0.0  1.0 4.0   15.0  ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 C H 0.0  1.0 16.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 C H 0.0  -1.0 1.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 C H 0.0  -1.0 2.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 C H 0.0  -1.0 4.0  15.0   ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff
symfunction_short C 3 C H 0.0  -1.0 16.0 15.0    ! central_atom type neighbor_atom1 neighbor_atom2 eta lambda zeta funccutoff

#######################################################################################################################
### fitting (mode 2):general inputs for short range AND electrostatic part:
########################################################################################################################
epochs 50
points_in_memory 500
mix_all_points
scale_symmetry_functions
center_symmetry_functions
fitting_unit  eV

########################################################################################################################
### fitting options ( mode 2): short range part only:
########################################################################################################################
optmode_short_energy 1
#optmode_short_force 1                   
short_energy_error_threshold 1.0
#short_force_error_threshold 0.8                 
kalman_lambda_short 0.98000
kalman_nue_short 0.99870
#use_old_weights_short               
#force_update_scaling -1.0d0          
#short_energy_group 1         
short_energy_fraction 1.0
#short_force_group 1                  
#short_force_fraction 0.05            
#use_short_forces                    
weights_min -1.0
weights_max 1.0
precondition_weights
repeated_energy_update
nguyen_widrow_weights_short

########################################################################################################################
### output options for mode 2 (fitting):  
########################################################################################################################
write_trainpoints
#write_trainforces      

########################################################################################################################
### output options for mode 3 (prediction):  
########################################################################################################################

calculate_hessian
calculate_frequencies
calculate_normalmodes
