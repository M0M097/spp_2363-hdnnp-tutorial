# BENCh Ring Lecture - Hands-on Tutorial
## Predicting Vibrational Frequencies of the Formic Acid Dimer

## Usage


This notebook is intended to run it on a binder hub, such as mybinder.org. 
There you just have to provide the link to this repository.

Running it on local hardware may result in incorrectly displayed 
animations. If you still want to try it, you need conda/mamba: 

```
conda env create -f binder/environment.yml
conda activate ase-runner-environment
jupyter-notebook handson.ipynb
```

## Authors

* [Moritz R. Schaefer](mailto:moritz.schaefer-f91@rub.de) - @M0M097
